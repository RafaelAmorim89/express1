#!/bin/bash

docker stop express1-test

# Delete the old repo
rm -rf /home/ec2-user/express1-test

# any future command that fails will exit the script
set -e

# BE SURE TO UPDATE THE FOLLOWING LINE WITH THE URL FOR YOUR REPO
git clone https://gitlab.com/RafaelAmorim89/express1.git express1-test

cd /home/ec2-user/express1-test

git checkout testing

# run the node app in a container
deploy-test/express1-test.sh -d
